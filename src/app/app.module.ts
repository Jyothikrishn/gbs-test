import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TopNavComponent } from './components/top-nav/top-nav.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { MainLayoutComponent } from './module/main-layout/main-layout.component';
import { DashboardComponent } from './module/main-layout/dashboard/dashboard.component';
import { MaterialModule } from './module/shared/material/material.module';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,

    // MainLayoutComponent,
    // DashboardComponent,
    // SideNavComponent,
    // TopNavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
