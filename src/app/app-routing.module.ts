import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './module/auth/auth.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { MainLayoutComponent } from './module/main-layout/main-layout.component';
import { DashboardComponent } from './module/main-layout/dashboard/dashboard.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: ()=> import('./module/main-layout/main-layout.module').then(m => m.MainLayoutModule)
  },
  {
    path: 'auth',
    loadChildren: () => import('./module/auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: '**',
    component: NotFoundComponent

  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
