import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.scss']
})
export class TopNavComponent implements OnInit {
  options = [
    "Upside Avenue",
    "Downside Avenue",
  ]
  constructor() { }

  ngOnInit() {
  }

}
