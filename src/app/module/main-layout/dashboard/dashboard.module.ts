import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { UnitsComponent } from './units/units.component';
import { GalleryItemComponent } from './gallery-item/gallery-item.component';
import { MaterialModule } from '../../shared/material/material.module';

@NgModule({
  declarations: [DashboardComponent, UnitsComponent, GalleryItemComponent],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild([{ path: '', component: DashboardComponent}])
  ]
})
export class DashboardModule { }
