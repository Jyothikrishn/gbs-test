import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainLayoutComponent } from './main-layout.component';
import { MainLayoutRoutingModule } from './main-layout-routing.module';
import { MaterialModule } from '../shared/material/material.module';


import { SideNavComponent } from 'src/app/components/side-nav/side-nav.component';
import { TopNavComponent } from 'src/app/components/top-nav/top-nav.component';

@NgModule({
  imports: [
    MaterialModule,
    CommonModule,
    MainLayoutRoutingModule,

  ],
  declarations: [
    MainLayoutComponent,
    SideNavComponent,
    TopNavComponent
  ],
})
export class MainLayoutModule { }
